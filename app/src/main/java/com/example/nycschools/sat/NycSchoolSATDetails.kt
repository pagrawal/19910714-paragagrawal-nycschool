package com.example.nycschools.sat

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nycschools.R
import com.example.nycschools.model.NycSchoolSATModel

@Composable
fun SchoolSATDetails(nycSchoolSatModel: NycSchoolSATModel) {
    Column(modifier = Modifier
        .fillMaxWidth()
        .background(color = Color.White)
        .padding(16.dp)
    ) {
        Text(
            text = nycSchoolSatModel.schoolName,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = stringResource(
                id = R.string.num_sat_test_takers,
                nycSchoolSatModel.satTestTakerCount
            ),
            fontSize = 16.sp,
            modifier = Modifier.padding(top = 6.dp)
        )
        Text(
            text = stringResource(
                id = R.string.sat_critical_reading_avg_score,
                nycSchoolSatModel.criticalSATAvgScore
            ),
            fontSize = 16.sp,
            modifier = Modifier.padding(top = 6.dp)
        )
        Text(
            text = stringResource(
                id = R.string.sat_math_avg_score,
                nycSchoolSatModel.mathAvgScore
            ),
            fontSize = 16.sp,
            modifier = Modifier.padding(top = 6.dp)
        )
        Text(
            text = stringResource(
                id = R.string.sat_writing_avg_score,
                nycSchoolSatModel.writingAvgScore
            ),
            fontSize = 16.sp,
            modifier = Modifier.padding(top = 6.dp)
        )
    }
}