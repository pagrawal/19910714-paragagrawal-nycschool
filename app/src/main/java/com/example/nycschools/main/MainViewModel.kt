package com.example.nycschools.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.model.NycSchoolsUiState
import com.example.nycschools.repository.MainRepository
import com.example.nycschools.repository.SATDataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val satDataRepository: SATDataRepository
): ViewModel() {

    private val _nycSATData: MutableStateFlow<NycSchoolsUiState> = MutableStateFlow(NycSchoolsUiState.Loading)
    var nycSATData: StateFlow<NycSchoolsUiState> = _nycSATData.asStateFlow()

    init {
        viewModelScope.launch {
            mainRepository.getNycSchools().collect {
                _nycSATData.value = it
            }
        }
    }

    fun onSchoolClick(schoolDBN: String) {
        viewModelScope.launch {
            satDataRepository.loadSATData(schoolDBN).collect{
                _nycSATData.value = it
            }
        }
    }
}