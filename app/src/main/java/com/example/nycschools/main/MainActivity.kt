package com.example.nycschools.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.nycschools.base.Loading
import com.example.nycschools.model.NycSchoolsUiState
import com.example.nycschools.sat.SchoolSATDetails
import com.example.nycschools.theme.NycSchoolsTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NycSchoolsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val data by viewModel.nycSATData.collectAsState()
                    when(data){
                        is NycSchoolsUiState.Loading -> {
                            Loading()
                        }
                        is NycSchoolsUiState.SchoolsDataSuccess -> {
                            NycSchoolInfo(
                                (data as NycSchoolsUiState.SchoolsDataSuccess).nycSchoolSatData,
                                ::onSchoolClick
                            )
                        }
                        is NycSchoolsUiState.Error -> {
                            Error((data as NycSchoolsUiState.Error).message)
                        }
                        is NycSchoolsUiState.SATDataSuccess -> {
                            SchoolSATDetails((data as NycSchoolsUiState.SATDataSuccess).nycSchoolSatModel)
                        }
                    }
                }
            }
        }
    }

    private fun onSchoolClick(schoolDBN: String) {
        viewModel.onSchoolClick(schoolDBN)
    }
}

@Composable
fun Error(name: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = name,
            color = Color.Red,
            fontSize = 16.sp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NycSchoolsTheme {
        Loading()
    }
}