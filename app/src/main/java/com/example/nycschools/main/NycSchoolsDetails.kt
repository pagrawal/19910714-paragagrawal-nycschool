package com.example.nycschools.main

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nycschools.model.NycSchoolsDataModel

@Composable
fun NycSchoolInfo(schools: List<NycSchoolsDataModel>, onSchoolClick: (String) -> Unit) {
    LazyColumn {
        schools.forEach {
            item {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.LightGray)
                    .clickable {
                        onSchoolClick(it.dbn)
                    }
                    .padding(16.dp)
                ) {
                    Text(
                        text = it.schoolName,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = it.location,
                        fontSize = 14.sp
                    )
                    Text(
                        text = it.phoneNumber,
                        fontSize = 14.sp
                    )
                    it.schoolEmail?.let { email ->
                        Text(
                            text = email,
                            fontSize = 14.sp
                        )
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}