package com.example.nycschools.repository

import com.example.nycschools.model.NycSchoolsUiState
import com.example.nycschools.module.NycSchoolsApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class SATDataRepository @Inject constructor(
    private val nycSchoolsApiService: NycSchoolsApiService
) {
    fun loadSATData(schoolDBN: String): Flow<NycSchoolsUiState> = flow {
        emit(NycSchoolsUiState.Loading)
        val result = nycSchoolsApiService.getNycSchoolSATData(schoolDBN)
        if(result.isSuccessful) {
            emit(NycSchoolsUiState.SATDataSuccess(result.body()!!.first()))
        } else {
            emit(NycSchoolsUiState.Error("Sorry! Something went wrong."))
        }
    }
}