package com.example.nycschools.repository

import com.example.nycschools.model.NycSchoolsUiState
import com.example.nycschools.module.NycSchoolsApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val nycSchoolsApiService: NycSchoolsApiService
) {
    fun getNycSchools(): Flow<NycSchoolsUiState> = flow {
        emit(NycSchoolsUiState.Loading)
        val result = nycSchoolsApiService.getNycSchoolsData()
        if(result.isSuccessful) {
            emit(NycSchoolsUiState.SchoolsDataSuccess(result.body()!!))
        } else {
            emit(NycSchoolsUiState.Error("Sorry! Something went wrong."))
        }
    }
}