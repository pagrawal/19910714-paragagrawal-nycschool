package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class NycSchoolSATModel(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("school_name")
    val schoolName: String,

    @SerializedName("num_of_sat_test_takers")
    val satTestTakerCount: String,

    @SerializedName("sat_critical_reading_avg_score")
    val criticalSATAvgScore: String,

    @SerializedName("sat_math_avg_score")
    val mathAvgScore: String,

    @SerializedName("sat_writing_avg_score")
    val writingAvgScore: String,
)
