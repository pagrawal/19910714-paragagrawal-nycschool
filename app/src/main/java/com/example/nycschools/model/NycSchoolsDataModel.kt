package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

data class NycSchoolsDataModel(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("school_name")
    val schoolName: String,

    @SerializedName("overview_paragraph")
    val overview: String,

    @SerializedName("location")
    val location: String,

    @SerializedName("phone_number")
    val phoneNumber: String,

    @SerializedName("school_email")
    val schoolEmail: String?,

)
