package com.example.nycschools.model

sealed interface NycSchoolsUiState {
    object Loading: NycSchoolsUiState

    data class SchoolsDataSuccess(
        val nycSchoolSatData: List<NycSchoolsDataModel>
    ): NycSchoolsUiState

    data class SATDataSuccess(
        val nycSchoolSatModel: NycSchoolSATModel
    ): NycSchoolsUiState

    data class Error(
        val message: String
    ): NycSchoolsUiState
}