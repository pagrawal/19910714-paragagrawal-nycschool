package com.example.nycschools.module

import com.example.nycschools.model.NycSchoolSATModel
import com.example.nycschools.model.NycSchoolsDataModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NycSchoolsApiService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getNycSchoolsData(): Response<List<NycSchoolsDataModel>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getNycSchoolSATData(
        @Query("dbn") dbn: String
    ): Response<List<NycSchoolSATModel>>
}